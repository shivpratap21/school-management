package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestSpring1Application {

	public static void main(String[] args) { 
		//Changes as per my task class-mang
		SpringApplication.run(TestSpring1Application.class, args);
	}

}
